export const OCCURRENCES_MOCK = [
    {
        description : "Ocorrencia 1",
        status : "Opened",
        picture : "./assets/imgs/occurrences/foto_1.jpg",
        coordinates : {
            lat: -15.874755799999988,
            long: -48.105175599999985
        }
    },
    {
        description : "Ocorrencia 1",
        status : "Opened",
        picture : "./assets/imgs/occurrences/foto_2.jpg",
        coordinates : {
            lat: -15.873755699999977,
            long: -48.103175599999975
        }
    },
    {
        description : "Ocorrencia 1",
        status : "Opened",
        picture : "./assets/imgs/occurrences/foto_3.jpg",
        coordinates : {
            lat: -15.872755599999966,
            long: -48.102175599999965
        }
    },
    {
        description : "Ocorrencia 1",
        status : "Resolved",
        picture : "./assets/imgs/occurrences/foto_4.jpg",
        coordinates : {
            lat: -15.871755499999955,
            long: -48.101175599999955
        }
    },
    {
        description : "Ocorrencia 1",
        status : "Resolved",
        picture : "./assets/imgs/occurrences/foto_5.jpg",
        coordinates : {
            lat: -15.870755399999944,
            long: -48.109175599999945
        }
    }
];
