import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {CameraPage} from "../pages/camera/camera";
import {Camera} from "@ionic-native/camera";
import {MapsPage} from "../pages/maps/maps";
import {Geolocation} from "@ionic-native/geolocation";
import {LaunchNavigator} from "@ionic-native/launch-navigator";
import {CityIssuePage} from "../pages/city-issue/city-issue";
import {OccurrenceFormPage} from "../pages/city-issue/occurrence-form/occurrence-form";
import {PrintProvider} from "../provider/print-provider";
import {BluetoothPrinterPage} from "../pages/bluetooth-printer/bluetooth-printer";
import {PrinterListModalPage} from "../pages/bluetooth-printer/printer-list-modal/printer-list-modal";
import {BluetoothSerial} from "@ionic-native/bluetooth-serial";
import {ViewImageModalPage} from "../pages/camera/view-image-modal/view-image-modal";
import {File} from "@ionic-native/file";
import {FileChooser} from "@ionic-native/file-chooser";
import {FilePath} from "@ionic-native/file-path";

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        CameraPage,
        MapsPage,
        CityIssuePage,
        OccurrenceFormPage,
        BluetoothPrinterPage,
        PrinterListModalPage,
        ViewImageModalPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        CameraPage,
        MapsPage,
        CityIssuePage,
        OccurrenceFormPage,
        BluetoothPrinterPage,
        PrinterListModalPage,
        ViewImageModalPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Camera,
        Geolocation,
        LaunchNavigator,
        PrintProvider,
        BluetoothSerial,
        File,
        FileChooser,
        FilePath,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {}
