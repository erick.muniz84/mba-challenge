import {CameraOptions} from "@ionic-native/camera";

export class CameraConfig {

    static getCameraConfig (camera) {
        return {
            quality: 100,
            destinationType: camera.DestinationType.DATA_URL,
            encodingType: camera.EncodingType.JPEG,
            mediaType: camera.MediaType.PICTURE
        };
    }
}

