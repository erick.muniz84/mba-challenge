import {CameraPage} from "../pages/camera/camera";
import {MapsPage} from "../pages/maps/maps";
import {CityIssuePage} from "../pages/city-issue/city-issue";
import {BluetoothPrinterPage} from "../pages/bluetooth-printer/bluetooth-printer";

export const pages = [
    { title: 'MBACam', component: CameraPage },
    { title: 'MBAMaps', component: MapsPage },
    { title: 'City Issue', component: CityIssuePage },
    { title: 'Bluetooth Printer', component: BluetoothPrinterPage }
];
