import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { File } from '@ionic-native/file';
import {FileChooser} from "@ionic-native/file-chooser";
import {FilePath} from "@ionic-native/file-path";

@Injectable()
export class PrintProvider {

    constructor(private btSerial:BluetoothSerial,
                private alertCtrl:AlertController,
                private file:File,
                private fileChooser:FileChooser,
                private filePath:FilePath) {
    }

    searchBt = () => {
        return this.btSerial.list();
    };

    connectBT = (address) => {
        return this.btSerial.connect(address);
    };

    //Get a file with fileChooser ionic plugin, and send to print via bluetooth
    printFile = (address) => {
        this.fileChooser.open().then(uri => {
            this.filePath.resolveNativePath(uri).then((path)=>{
                let fileName = /(?=[\w-]+\.\w{3,4}$).+/g.exec(path)[0];
                let newPath = path.replace(fileName, "");

                this.file.readAsArrayBuffer(newPath, fileName).then((data)=>{
                    let printData = data;

                    let bluetoothConnection = this.connectBT(address).subscribe(connectionData => {
                        this.btSerial.write(printData).then(data => {
                            let printModal = this.alertCtrl.create({
                                title:"Document has been printed!",
                                buttons:['Dismiss']
                            });

                            printModal.present();
                            bluetoothConnection.unsubscribe();

                        }, error =>{
                            let printModal = this.alertCtrl.create({
                                title:"An error has occurred to print document " + error,
                                buttons:['Dismiss']
                            });

                            printModal.present();
                        });
                    }, error => {
                        let printModal = this.alertCtrl.create({
                            title:"Any error has occurred " + error,
                            buttons:['Dismiss']
                        });

                        printModal.present();
                    });
                }).catch(err=>console.log(err));
            })
        });
    };
}
