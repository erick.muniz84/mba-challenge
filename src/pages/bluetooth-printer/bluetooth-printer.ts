import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import { PrinterListModalPage } from "./printer-list-modal/printer-list-modal";
import { PrintProvider } from "../../provider/print-provider";

@Component({
    selector: 'bluetooth-printer',
    templateUrl: 'bluetooth-printer.html'
})
export class BluetoothPrinterPage {

    selectedPrinter:any = [];

    constructor(public navCtrl: NavController,
                private modalCtrl:ModalController,
                private printProvider:PrintProvider,
                private alertCtrl:AlertController) {
    }

    //List all bluetooth devices available to connect
    listBTDevice = () => {
        this.printProvider.searchBt().then(dataList => {
            let devicesModal = this.modalCtrl.create(PrinterListModalPage,{data:dataList});

            devicesModal.onDidDismiss(data => {
                this.selectedPrinter = data;

                let modal = this.alertCtrl.create({
                    title: data.name + " selected",
                    buttons:['Dismiss']
                });

                modal.present();
            });

            devicesModal.present();
        }, err => {
            let modal = this.alertCtrl.create({
                title:"ERROR " + err,
                buttons:['Dismiss']
            });

            modal.present();
        });
    };

    //Device Connection Tester
    testConnectPrinter = () => {
        let deviceId = this.selectedPrinter.id;

        if( deviceId == null || deviceId == "" || deviceId == undefined) {
            // TODO: implement if the printer ID is undefined
        } else {
            let connection = this.printProvider.connectBT(deviceId).subscribe(data => {
                let modal = this.alertCtrl.create({
                    title : "Connect successful",
                    buttons : ['Dismiss']
                });

                modal.present();

            }, err => {
                let modal = this.alertCtrl.create({
                    title : "ERROR " + err,
                    buttons : ['Dismiss']
                });

                modal.present();
            });
        }
    };

    //Verify if selected print is not undefined, and send file to print
    printPDFFile = () => {
        let id = this.selectedPrinter.id;

        if (id === null || id === "" || id === undefined) {
            // TODO: implement if the printer is undefined
        } else {
            this.printProvider.printFile(id);
        }
    };
}
