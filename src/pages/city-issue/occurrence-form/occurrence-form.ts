import {Component} from '@angular/core';
import {ViewController} from "ionic-angular";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {CameraConfig} from "../../../constant/camera-config";

@Component({
    selector: 'occurrence-form',
    templateUrl: 'occurrence-form.html'
})
export class OccurrenceFormPage {

    occurrence:any = {
        picture :  "./assets/imgs/no-photo-yet.gif"
    };

    cameraOptions:CameraOptions = CameraConfig.getCameraConfig(this.camera);

    constructor(public viewCtrl: ViewController,
                private camera: Camera) {
    }

    handleClickDone = () => {
        this.viewCtrl.dismiss(this.occurrence);
    };

    handleClickTakeAPic = () => {
        this.camera.getPicture(this.cameraOptions).then((imageData) => {
            this.occurrence.picture = 'data:image/jpeg;base64,' + imageData;
        }, (err) => {
            // Handle error
        });
    };

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
