import {Component, ElementRef, ViewChild} from '@angular/core';
import {ModalController, NavController, Platform} from 'ionic-angular';
import {Geolocation} from "@ionic-native/geolocation";
import {OccurrenceFormPage} from "./occurrence-form/occurrence-form";
import L from "leaflet";
import {OCCURRENCES_MOCK} from "../../mock/occurrences";

@Component({
    selector: 'city-issue',
    templateUrl: 'city-issue.html'
})
export class CityIssuePage {

    @ViewChild('cityIssuePageMapContainer') mapContainer: ElementRef;
    map: any;

    currentLocation;

    occurrences = Object.assign([], OCCURRENCES_MOCK);

    constructor(public navCtrl: NavController,
                private geolocation: Geolocation,
                public platform: Platform,
                public modalCtrl: ModalController) {
    }

    ionViewDidLoad = () => {
        this.getCurrentLocation();
    };

    ionViewDidEnter = () => {
        this.buildMap();
    };

    ionViewWillLeave = () => {
        this.map.remove();
    };

    //Get current position from device and center map at feature
    getCurrentLocation = () => {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.currentLocation = resp;
            this.centerMap();
            this.plotSavedOccurrencesOnMap();
        }).catch((error) => {
            console.log('Error getting location', error);
        });
    };

    buildMap = () => {
        this.map = L.map("cityIssuePageMapContainer").fitWorld();
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attributions: 'www.tphangout.com'
        })
            .addTo(this.map);
    };

    /**
     *
     * @param point
     * @desc Center map at any point if exists params, otherwise center on current device's position
     */
    centerMap = (point?) => {
        let lat;
        let long;

        if (!point) {
            lat     = this.currentLocation.coords.latitude;
            long    = this.currentLocation.coords.longitude;
        } else {
            lat     = point.lat;
            long    = point.long;
        }

        this.map.setView([lat, long], 15);
    };

    handleClickAddOccurrence = () => {
        const modal = this.modalCtrl.create(OccurrenceFormPage);

        modal.onDidDismiss((data) => {
            if (Boolean(data)) {
                let occurrence = Object.assign({}, data, {
                    status : "Opened",
                    coordinates : {
                        lat : this.currentLocation.coords.latitude,
                        long : this.currentLocation.coords.longitude
                    }});

                this.occurrences.push(occurrence);
                this.addPointOnMap(occurrence);
                this.centerMap(occurrence.coordinates);
            }
        });

        modal.present();
    };

    plotSavedOccurrencesOnMap = () => {
        this.occurrences.forEach((occurrence) => {
            this.addPointOnMap(occurrence);
        });
    };

    addPointOnMap = (occurrence) => {
        let lat     = occurrence.coordinates.lat;
        let long    = occurrence.coordinates.long;

        L.marker([lat, long])
            .addTo(this.map)
            .bindPopup(this.getPopupContent(occurrence));
    };

    getPopupContent = (occurrence) => {
        let content =
            `<div class="city-issue-popup-content">
                <div>
                    <strong>Description:</strong>${occurrence.description}
                </div>
                <div>
                    <strong>Status:</strong>${occurrence.status}
                </div>
                
                <img src="${occurrence.picture}" >
            </div>`;

        return content;
    };
}
