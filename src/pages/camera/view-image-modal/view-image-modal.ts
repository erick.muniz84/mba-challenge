import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

@Component({
    selector: 'view-image-modal',
    templateUrl: 'view-image-modal.html'
})
export class ViewImageModalPage {

    image = "";

    constructor(public navParams: NavParams,
                public viewCtrl: ViewController) {

        this.image =  this.navParams.get('image');
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }
}
