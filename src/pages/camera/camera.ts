import { Component } from '@angular/core';
import {ModalController, NavController} from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import {CameraConfig} from "../../constant/camera-config";
import {GALLERY_MOCK} from "../../mock/gallery";
import {ViewImageModalPage} from "./view-image-modal/view-image-modal";

@Component({
    selector: 'camera',
    templateUrl: 'camera.html'
})
export class CameraPage {

    gallery = Object.assign([], GALLERY_MOCK);

    cameraOptions:CameraOptions = CameraConfig.getCameraConfig(this.camera);

    constructor(public navCtrl: NavController,
                private camera: Camera,
                public modalCtrl: ModalController) {
    }

    handleClickTakeAPic = () => {
        this.camera.getPicture(this.cameraOptions).then((imageData) => {
            this.gallery.push('data:image/jpeg;base64,' + imageData);
        }, (err) => {
            // Handle error
        });
    };

    handleClickOnAnyImage = (imgSrc) => {
        let modal = this.modalCtrl.create(ViewImageModalPage, {
            image : imgSrc
        });

        modal.present();
    };
}


