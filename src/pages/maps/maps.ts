import {Component, ElementRef, ViewChild} from '@angular/core';
import { NavController } from 'ionic-angular';
import { Geolocation } from "@ionic-native/geolocation";
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import L from 'leaflet';
import { Platform } from 'ionic-angular';

@Component({
    selector: 'maps',
    templateUrl: 'maps.html'
})
export class MapsPage {

    @ViewChild('mapsPageMapContainer') mapContainer: ElementRef;
    map: any;

    currentLocation;

    constructor(public navCtrl: NavController,
                private geolocation: Geolocation,
                private launchNavigator: LaunchNavigator,
                public platform: Platform) {
    }

    ionViewDidLoad () {
        this.getCurrentLocation();
    }

    ionViewDidEnter = () => {
        this.buildMap();
    };

    ionViewWillLeave = () => {
        this.map.remove();
    };

    getCurrentLocation = () => {
        this.geolocation.getCurrentPosition().then((resp) => {
            this.currentLocation = resp;
            this.setCurrentPosition();
        }).catch((error) => {
            console.log('Error getting location', error);
        });
    };

    buildMap = () => {
        this.map = L.map("mapsPageMapContainer").fitWorld();
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attributions: 'www.tphangout.com'
        })
            .addTo(this.map);
    };

    setCurrentPosition = () => {
        let lat     = this.currentLocation.coords.latitude;
        let long    = this.currentLocation.coords.longitude;

        L.marker([lat, long])
            .addTo(this.map);

        this.map.setView([lat, long], 15);
    };

    //This method open another map app using ionic native launcher
    handleClickOpenOnMapApplication = () => {
        let lat     = this.currentLocation.coords.latitude;
        let long    = this.currentLocation.coords.longitude;
        let options: LaunchNavigatorOptions = {
            start: "MBA Mobi, St. de Habitações Individuais Sul QI 13 - Lago Sul, Brasília - DF, 71635-130",
            app: this.platform.is("ios") ? this.launchNavigator.APP.APPLE_MAPS : this.launchNavigator.APP.GOOGLE_MAPS
        };

        this.launchNavigator.navigate([lat, long], options)
            .then(
                success => console.log('Launched navigator'),
                error => console.log('Error launching navigator', error)
            );
    };
}
